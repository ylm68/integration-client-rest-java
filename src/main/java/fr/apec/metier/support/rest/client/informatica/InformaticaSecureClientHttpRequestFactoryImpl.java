package fr.apec.metier.support.rest.client.informatica;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;

import java.io.IOException;
import java.net.URI;

/**
 * Created by pleroux on 14/02/2018.
 */
public class InformaticaSecureClientHttpRequestFactoryImpl
    implements ClientHttpRequestFactory, InformaticaSecureClientHttpRequestFactory {

  private static final Logger LOGGER = LoggerFactory.getLogger(InformaticaSecureClientHttpRequestFactoryImpl.class);

  private String token;

  private ClientHttpRequestFactory clientHttpRequestFactory = new SimpleClientHttpRequestFactory();

  @Override
  public ClientHttpRequest createRequest(URI uri, HttpMethod httpMethod) throws IOException {
    ClientHttpRequest request = clientHttpRequestFactory.createRequest(uri, httpMethod);
    request.getHeaders().add("X-Auth-Token", token);
    /*request.getHeaders().add("Accept", MediaType.APPLICATION_JSON_VALUE);
    request.getHeaders().add("Content-Type", MediaType.APPLICATION_JSON_VALUE);*/
    return request;
  }

  @Override
  public String computeToken(URI uri, String username, String password) throws IOException {
    ClientHttpRequest request = clientHttpRequestFactory.createRequest(uri, HttpMethod.POST);
    request.getHeaders().add("X-Username", username);
    request.getHeaders().add("X-Password", password);
    ClientHttpResponse response = request.execute();
    if (response.getStatusCode() == HttpStatus.OK) {
      String token = response.getHeaders().getFirst("X-Auth-Token");
      LOGGER.info(String.format("token computed = [%s]", token));
      return token;
    } else {
      LOGGER.warn(String.format("Init token failed (%s/%s)...", username, password));
      return null;
    }
  }

  @Override
  public String getToken() {
    return token;
  }

  @Override
  public void setToken(String token) {
    this.token = token;
  }

  @Override
  public void resetToken() {
    LOGGER.debug("resetToken");
    this.token = null;
  }

}
