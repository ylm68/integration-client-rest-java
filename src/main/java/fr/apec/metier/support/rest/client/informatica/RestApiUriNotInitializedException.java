package fr.apec.metier.support.rest.client.informatica;

/**
 * Created by pleroux on 14/02/2018.
 */
public class RestApiUriNotInitializedException extends RuntimeException {

  @Override
  public String getMessage() {
    return "Rest API URI not initialized!...";
  }

}
