package fr.apec.metier.support.rest.client.informatica;

import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;

import java.io.IOException;
import java.net.URI;

/**
 * Created by pleroux on 14/02/2018.
 */
public interface InformaticaSecureClientHttpRequestFactory {

  ClientHttpRequest createRequest(URI uri, HttpMethod httpMethod) throws IOException;

  String computeToken(URI uri, String username, String password) throws IOException;

  String getToken();

  void setToken(String token);

  void resetToken();

}
