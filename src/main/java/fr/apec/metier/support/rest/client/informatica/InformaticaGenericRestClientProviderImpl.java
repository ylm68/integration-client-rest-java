package fr.apec.metier.support.rest.client.informatica;

import fr.apec.metier.service.cachemanager.CacheManager;
import fr.apec.metier.service.property.PropertyService;
import fr.apec.metier.support.rest.client.ApecResponseErrorHandler;
import fr.apec.metier.support.rest.client.CacheClientRT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Created by pleroux on 14/02/2018.
 */
public class InformaticaGenericRestClientProviderImpl<T extends CacheClientRT>
    implements InformaticaGenericRestClientProvider<T> {

  private static final Logger LOGGER = LoggerFactory.getLogger(InformaticaGenericRestClientProviderImpl.class);

  protected String wsmRestApiUri;
  protected String pathService;
  protected T serviceClientRT;

  public InformaticaGenericRestClientProviderImpl(String pathService, T serviceClientRT) {
    this.pathService = pathService;
    this.serviceClientRT = serviceClientRT;
  }

  public void setWsmRestApiUri(String wsmRestApiUri) {
    this.wsmRestApiUri = wsmRestApiUri;
  }

  @Override
  public String getToken() {
    return ((InformaticaSecureClientHttpRequestFactory) (getClient().getRestTemplate().getRequestFactory())).getToken();
  }

  @Override
  public void setToken(String token) {
    ((InformaticaSecureClientHttpRequestFactory) (getClient().getRestTemplate().getRequestFactory())).setToken(token);
  }

  protected void resetToken() {
    ((InformaticaSecureClientHttpRequestFactory) (serviceClientRT.getRestTemplate().getRequestFactory())).resetToken();
  }

  @Override
  public T getClient() throws RestApiUriNotInitializedException {
    if (wsmRestApiUri == null) throw new RestApiUriNotInitializedException();
    return serviceClientRT;
  }

  @Override
  public T getClientInitializedWithoutToken() throws RestApiUriNotInitializedException {
    LOGGER.debug("getClientInitializedWithoutToken");
    if (wsmRestApiUri == null) throw new RestApiUriNotInitializedException();

    PropertyService propertyService = new PropertyService() {
      @Override
      public String getProperty(String key) {
        return String.format("%s%s", wsmRestApiUri, pathService);
      }

      @Override
      public String getService(String serviceKey) {
        return getProperty(serviceKey);
      }

      @Override
      public Map<String, String> getProperties() {
        return null;
      }
    };

    CacheManager cacheManager = new CacheManager() {
      @Override
      public Object get(String key) {
        return null;
      }

      @Override
      public void put(String key, Object item) {
      }

      @Override
      public boolean remove(String key) {
        return false;
      }
    };

    serviceClientRT.setCacheManager(cacheManager);
    serviceClientRT.setPropertyService(propertyService);
    serviceClientRT.getRestTemplate().setErrorHandler(new ApecResponseErrorHandler());
    return serviceClientRT;
  }

  @Override
  public String computeToken(String username, String password) throws RestApiUriNotInitializedException, IOException, URISyntaxException {
    LOGGER.debug("computeToken");
    if (wsmRestApiUri == null) throw new RestApiUriNotInitializedException();
    URI wsmUri = new URI(wsmRestApiUri);
    return ((InformaticaSecureClientHttpRequestFactory) (serviceClientRT.getRestTemplate().getRequestFactory()))
        .computeToken(wsmUri, username, password);
  }

  @Override
  public T getClientInitializedWithToken(String username, String password) throws RestApiUriNotInitializedException {
    LOGGER.debug("getClientInitializedWithToken");
    serviceClientRT = getClientInitializedWithoutToken();
    // Récupération du token d'authentification pour Informatica :
    try {
      setToken(computeToken(username, password));
    } catch (IOException e) {
      LOGGER.warn(e.getMessage());
      resetToken();
    } catch (URISyntaxException e) {
      LOGGER.warn(e.getMessage());
      resetToken();
    }
    return serviceClientRT;
  }

  @Override
  public T getClientInitializedWithToken(String token) throws RestApiUriNotInitializedException {
    LOGGER.debug("getClientInitializedWithToken");
    serviceClientRT = getClientInitializedWithoutToken();
    // Récupération du token d'authentification pour Informatica :
    setToken(token);
    return serviceClientRT;
  }

}
