package fr.apec.metier.support.rest.client.integration;

import fr.apec.metier.support.rest.client.informatica.InformaticaSecureClientHttpRequestFactoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by pleroux on 14/02/2018.
 */
public class App {

  private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

  public String AUTH_TOKEN = "";
  public String URL = System.getProperty("wsmRestApi.Uri");

  public double in_ID_OFFRE;

  public double getIn_ID_OFFRE() {
    return in_ID_OFFRE;
  }

  public void setIn_ID_OFFRE(double in_ID_OFFRE) {
    this.in_ID_OFFRE = in_ID_OFFRE;
  }

  public void setAuthToken(String token) {
    AUTH_TOKEN = token;
  }

  public String getUrl() {
    return URL;
  }

  public void initContext(String username, String password) {
    if (isApiUriOK()) {
      initToken(username, password);
    } else {
      LOGGER.warn(String.format("REST API URI malformed! : [%s]", getUrl()));
    }
  }

  public boolean isApiUriOK() {
    String urlPattern = "http://.*/rest-server/rest/";
    Pattern pattern = Pattern.compile(urlPattern);
    Matcher matcher = pattern.matcher(getUrl());
    return matcher.matches();
  }

  public void initToken(String username, String password) {
    setAuthToken(computeToken(username, password));
  }

  public String computeToken(String username, String password) {
    InformaticaSecureClientHttpRequestFactoryImpl informaticaSecureClientHttpRequestFactory =
        new InformaticaSecureClientHttpRequestFactoryImpl();
    try {
      return informaticaSecureClientHttpRequestFactory.computeToken(new URI(URL), username, password);
    } catch (IOException e) {
      return "";
    } catch (URISyntaxException e) {
      return "";
    }
  }

  public void logInfo(String msg) {
    LOGGER.info(msg);
  }

  public void logError(String msg) {
    LOGGER.warn(msg);
  }

  public void generateRow() {
  }

}
