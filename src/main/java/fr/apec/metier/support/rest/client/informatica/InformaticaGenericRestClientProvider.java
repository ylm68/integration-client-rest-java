package fr.apec.metier.support.rest.client.informatica;

import fr.apec.metier.support.rest.client.CacheClientRT;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by pleroux on 14/02/2018.
 */
public interface InformaticaGenericRestClientProvider<T extends CacheClientRT> {

  String getToken();

  void setToken(String token);

  T getClient() throws RestApiUriNotInitializedException;

  T getClientInitializedWithoutToken() throws RestApiUriNotInitializedException;

  String computeToken(String username, String password) throws RestApiUriNotInitializedException, IOException, URISyntaxException;

  T getClientInitializedWithToken(String username, String password) throws RestApiUriNotInitializedException;

  T getClientInitializedWithToken(String token) throws RestApiUriNotInitializedException;

}
